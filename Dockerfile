FROM registry.gitlab.com/gitlab-org/build/cng/gitlab-kas:v16.11.10 as kas

FROM gcr.io/distroless/base-debian12:debug-nonroot-amd64

COPY --from=kas /usr/bin/kas /usr/bin/kas

ENTRYPOINT ["/usr/bin/kas"]
